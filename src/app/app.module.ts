import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { PageProfilesComponent } from './pages/profiles/profiles.component'
import { PageProfileComponent } from './pages/profile/profile.component'
import { ProfilesTableComponent } from './components/profiles/table/table.component'
import { ProfilesSearchComponent } from './components/profiles/search/search.component'

import { MatInputModule } from '@angular/material/input'
import { MatIconModule } from '@angular/material/icon'
import { MatToolbarModule } from '@angular/material/toolbar'
import { StoreModule } from '@ngrx/store'
import { profilesReducer } from '@store/reducers/profile.reducers'
import { EffectsModule } from '@ngrx/effects'
import { ProfileEffects } from '@store/effects/profile.effects'
import { HttpClientModule } from '@angular/common/http'
import { MatTableModule } from '@angular/material/table'
import { MatPaginatorModule } from '@angular/material/paginator'
import { MatSortModule } from '@angular/material/sort'
import { MatButtonModule } from '@angular/material/button'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ProfileHeaderComponent } from './components/profile/header/header.component'
import { ProfileBodyComponent } from './components/profile/body/body.component'
import { MatGridListModule } from '@angular/material/grid-list'
import { ProfileFactsComponent } from './components/profile/facts/facts.component'
import { MatTabsModule } from '@angular/material/tabs'
import { MatCardModule } from '@angular/material/card'
import { MatDividerModule } from '@angular/material/divider'
import { MatListModule } from '@angular/material/list'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { ProfileDetailComponent } from './components/profile/detail/detail.component'
import { LoaderComponent } from './components/shared/loader/loader.component'
import { AlertComponent } from './components/shared/alert/alert.component'
import { MatDialogModule } from '@angular/material/dialog'
import { TruncPipe } from '@pipes/trunc.pipe'
import { InitialsPipe } from '@pipes/initials.pipe'

@NgModule({
  declarations: [
    AppComponent,
    PageProfilesComponent,
    PageProfileComponent,
    ProfilesTableComponent,
    ProfilesSearchComponent,
    ProfileHeaderComponent,
    ProfileBodyComponent,
    ProfileFactsComponent,
    ProfileDetailComponent,
    LoaderComponent,
    AlertComponent,
    TruncPipe,
    InitialsPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      profiles: profilesReducer
    }),
    EffectsModule.forRoot([ProfileEffects]),
    MatGridListModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDialogModule
  ],
  entryComponents: [AlertComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { select, Store } from '@ngrx/store'
import { State } from '@store/reducers/profile.reducers'
import { Get_Profiles } from '@store/actions/profile.actions'
import { MatDialog } from '@angular/material/dialog'
import { AlertComponent } from '@components/shared/alert/alert.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  profile$: Observable<any>
  showLoader: boolean
  constructor(private store: Store<State>, public dialog: MatDialog) {
    this.showLoader = true
    this.store.dispatch(Get_Profiles())
  }

  ngOnInit() {
    this.profile$ = this.store.pipe(select('profiles'))
    this.profile$.subscribe(data => {
      if (data.complete && !data.progress) {
        this.showLoader = false
      } else {
        this.showLoader = true
      }

      if (data.error) {
        this.openDialog(data.message)
      }
    })
  }

  openDialog(message) {
    if (!this.dialog.getDialogById('500'))
      this.dialog.open(AlertComponent, {
        data: { message, errorType: 'warning' },
        id: '500'
      })
  }
}

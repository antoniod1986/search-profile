export interface Profile {
	address: string,
	birthdate: string,
	email: string,
	email2?: string,
	first_name: string,
	gender: string,
	last_name: string,
	localid: string,
	loyalty_member_id: string,
	modified?: Date,
	phone: string,
	photo?: string,
	prefix?: string,
	suffix?: string
}
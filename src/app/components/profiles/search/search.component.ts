import { Component, OnInit, EventEmitter, Output } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'profiles-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class ProfilesSearchComponent implements OnInit {
  searchForm: FormGroup
  @Output() searchEvent = new EventEmitter<Event>()

  constructor() {}

  ngOnInit() {
    this.searchForm = new FormGroup({
      filter: new FormControl(null, null)
    })
  }

  onSubmit() {
    this.searchEvent.emit(this.searchForm.get('filter').value)
  }
}

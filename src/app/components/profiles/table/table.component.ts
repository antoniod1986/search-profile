import { Component, OnInit, ViewChild, Input } from '@angular/core'
import { Profile } from '@models/profile'
import { Observable } from 'rxjs'
import { select, Store } from '@ngrx/store'
import { State } from '@store/reducers/profile.reducers'
import { MatPaginator } from '@angular/material/paginator'
import { MatSort } from '@angular/material/sort'
import { MatTableDataSource } from '@angular/material/table'
import { MatDialog } from '@angular/material/dialog'
import { AlertComponent } from '@components/shared/alert/alert.component'

@Component({
  selector: 'profiles-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class ProfilesTableComponent implements OnInit {
  private profiles$: any
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
  @ViewChild(MatSort, { static: true }) sort: MatSort
  displayedColumns: string[] = [
    'avatar',
    'id',
    'email',
    'name',
    'phone',
    'address',
    'modified',
    'view'
  ]
  dataSource: MatTableDataSource<Profile>

  constructor(private store: Store<State>, public dialog: MatDialog) {}
  applyFilter(filterValue: string) {
    this.dataSource.filter = (filterValue || '').trim().toLowerCase()
    if (this.dataSource.filteredData.length === 0) {
      this.dialog.open(AlertComponent, {
        data: { message: 'Profile not found!' }
      })
    }
  }

  ngOnInit() {
    this.profiles$ = this.store.pipe(select('profiles')).subscribe(data => {
      this.dataSource = new MatTableDataSource(data.profiles || [])
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    })
  }

  ngOnDestroy() {
    this.profiles$.unsubscribe()
  }
}

import { Component, OnInit } from '@angular/core'
import { Profile } from 'selenium-webdriver/firefox'
import { Observable } from 'rxjs'
import { select, Store } from '@ngrx/store'
import { State } from '@store/reducers/profile.reducers'

@Component({
  selector: 'profile-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class ProfileHeaderComponent implements OnInit {
  profile$: Observable<Profile>
  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.profile$ = this.store.pipe(select('profiles'))
  }
}

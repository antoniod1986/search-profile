import { Component, OnInit } from '@angular/core'
import { Profile } from 'selenium-webdriver/firefox'
import { Observable } from 'rxjs'
import { select, Store } from '@ngrx/store'
import { State } from '@store/reducers/profile.reducers'

@Component({
  selector: 'profile-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class ProfileDetailComponent implements OnInit {
  profile$: any
  items = [
    { label: 'Prefix', value: '-', key: 'prefix' },
    { label: 'First Name', value: '-', key: 'first_name' },
    { label: 'Last Name', value: '-', key: 'last_name' },
    { label: 'Suffix', value: '-', key: 'suffix' },
    { label: 'Loialty Member ID', value: '-', key: 'loyalty_member_id' },
    { label: 'Phone', value: '-', key: 'phone' },
    { label: 'Address', value: '-', key: 'address' },
    { label: 'Birthdate', value: '-', key: 'birthdate' },
    { label: 'Language Preference', value: '-', key: 'lang' }
  ]
  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.profile$ = this.store.pipe(select('profiles')).subscribe(data => {
      if (data.complete) {
        if (data.profile) {
          this.items = this.items.map(item => {
            return { ...item, value: data.profile[item.key] || '-' }
          })
        }
      }
    })
  }

  ngOnDestroy() {
    this.profile$.unsubscribe()
  }
}

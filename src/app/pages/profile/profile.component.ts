import { Component, OnInit, OnDestroy } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { State } from '@store/reducers/profile.reducers'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { Get_Profile } from '@store/actions/profile.actions'
import { flatMap } from 'rxjs/operators'
import { of } from 'rxjs'

@Component({
  selector: 'page-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class PageProfileComponent implements OnInit, OnDestroy {
  private profile$: any
  private route$: any
  private id: string

  constructor(private store: Store<State>, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route$ = this.route.paramMap
      .pipe(
        flatMap((params: ParamMap) => {
          return of(params.get('id'))
        })
      )
      .subscribe(id => {
        this.id = id
      })

    this.profile$ = this.store.pipe(select('profiles')).subscribe(data => {
      const profile = data.profile || {}
      if (data.complete && !data.error && profile.localid != this.id) {
        this.store.dispatch(
          Get_Profile({
            id: this.id
          })
        )
      }
    })
  }

  ngOnDestroy() {
    this.route$.unsubscribe()
    this.profile$.unsubscribe()
  }
}

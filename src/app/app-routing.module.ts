import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { PageProfilesComponent } from '@pages/profiles/profiles.component'
import { PageProfileComponent } from '@pages/profile/profile.component'

const routes: Routes = [
  { path: 'profiles', component: PageProfilesComponent },
  { path: '', pathMatch: 'full', redirectTo: '/profiles' },
  { path: 'profile/:id', component: PageProfileComponent },
  { path: '**', redirectTo: '/profiles' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

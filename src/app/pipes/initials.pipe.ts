import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'initials' })
export class InitialsPipe implements PipeTransform {
  transform(value: string, forceTrunc: boolean = false): string {
    let newStr: string = value.trim()
    if (value.length === 0 || value === '-') return ''
    if (forceTrunc) newStr = newStr.slice(0, 3)
    else if (value.length > 3) return newStr
    return `${newStr}.`
  }
}

import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'trunc' })
export class TruncPipe implements PipeTransform {
  transform(value: string, limit: number): string {
    let newStr: string = value
    if (value === '-') return value
    if (value.length > limit) {
      newStr = `${value.slice(0, limit).trim()}...`
    }
    return newStr
  }
}

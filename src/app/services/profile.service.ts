import { Injectable } from '@angular/core'
import { Observable, throwError } from 'rxjs'
import { retry, catchError } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http'
import { Profile } from '@models/profile'

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private readonly API_URL = 'https://profiles-list.firebaseio.com/Data.json'

  constructor(private http: HttpClient) {}

  getAll(): Observable<Profile[]> {
    return this.http.get<Profile[]>(this.API_URL).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError() {
    let errorMessage = 'Internal Server Error'

    return throwError(errorMessage)
  }
}

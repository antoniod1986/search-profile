import { TestBed } from '@angular/core/testing'
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing'

import { ProfileService } from './profile.service'

describe('ProfileService', () => {
  function setup() {
    const profileService = TestBed.get(ProfileService)
    const httpTestingController = TestBed.get(HttpTestingController)
    return { profileService, httpTestingController }
  }
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [ProfileService],
      imports: [HttpClientTestingModule]
    })
  )

  it('should be created', () => {
    const service: ProfileService = TestBed.get(ProfileService)
    expect(service).toBeTruthy()
  })

  it('should call search api', () => {
    const { profileService, httpTestingController } = setup()
    const mockResponse = []
    const API_URL = 'https://profiles-list.firebaseio.com/Data.json'

    profileService.getAll().subscribe(data => {
      expect(data.mapData).toEqual(mockResponse)
    })
    const req = httpTestingController.expectOne(API_URL)
    expect(req.request.method).toBe('GET')
    req.flush({
      mapData: mockResponse
    })
  })
})

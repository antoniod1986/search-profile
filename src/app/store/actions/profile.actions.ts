import { createAction, props } from '@ngrx/store'

export const Get_Profiles = createAction('[Profile] Get Profiles')
export const Get_Profiles_Success = createAction(
  '[Profile] Get Profiles Success',
  props<{ payload: any }>()
)
export const Get_Profiles_Error = createAction(
  '[Profile] Get Profiles Error',
  props<{ payload: any }>()
)

export const Get_Profile = createAction(
  '[Profile] Get Profile',
  props<{ id: any }>()
)

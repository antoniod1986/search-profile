import { Action, createReducer, on } from '@ngrx/store'
import * as ProfileActions from '@store/actions/profile.actions'

export interface State {
  progress: boolean
  error: boolean
  complete: boolean
  profiles: []
  profile: any
}
export const initialState: State = {
  progress: false,
  error: false,
  complete: false,
  profiles: [],
  profile: null
}

const _profilesReducer = createReducer(
  initialState,
  on(ProfileActions.Get_Profiles, state => ({
    ...state,
    progress: true,
    error: false
  })),
  on(ProfileActions.Get_Profiles_Success, (state, action) => ({
    ...state,
    profiles: action.payload,
    progress: false,
    error: false,
    complete: true
  })),
  on(ProfileActions.Get_Profiles_Error, (state, action) => ({
    ...state,
    profiles: [],
    progress: false,
    error: true,
    complete: true,
    message: action.payload
  })),
  on(ProfileActions.Get_Profile, (state, action) => {
    const profile = state.profiles.find((item: any) => {
      return item.localid == action.id
    })
    if (profile) {
      return {
        ...state,
        error: false,
        profile: profile
      }
    } else {
      return {
        ...state,
        error: true,
        message: 'Profile not found!'
      }
    }
  })
)

export function profilesReducer(state: State | undefined, action: Action) {
  return _profilesReducer(state, action)
}

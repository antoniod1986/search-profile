import { Injectable } from '@angular/core'
import { Actions, Effect, ofType } from '@ngrx/effects'
import {
  catchError,
  debounceTime,
  map,
  switchMap,
  withLatestFrom
} from 'rxjs/operators'
import * as ProfileActions from '@store/actions/profile.actions'
import { Store, select } from '@ngrx/store'
import { Profile } from '@models/profile'
import { of, Observable } from 'rxjs'
import { ProfileService } from '@services/profile.service'
import { State } from '@store/reducers/profile.reducers'

@Injectable({
  providedIn: 'root'
})
export class ProfileEffects {
  constructor(
    private actions$: Actions,
    private profileService: ProfileService,
    private store: Store<State>
  ) {}

  @Effect()
  getProfiles = this.actions$.pipe(
    ofType(ProfileActions.Get_Profiles),
    debounceTime(300),
    switchMap(() => {
      return this.profileService.getAll().pipe(
        map((response: Profile[]) => ({
          type: '[Profile] Get Profiles Success',
          payload: response
        })),
        catchError(error =>
          of({
            type: '[Profile] Get Profiles Error',
            payload: error
          })
        )
      )
    })
  )
}
